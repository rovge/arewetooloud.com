FROM node:10

WORKDIR /usr/arewetooloud

COPY package.json ./
COPY package-lock.json ./

RUN npm i

COPY . .

EXPOSE 80

CMD ["npm", "run", "start"]
