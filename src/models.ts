export interface IComplaint {
    name?: string;
    message: string;
    date: Date;
}

export interface ITooLoud {
    tooLoud: boolean;
}
