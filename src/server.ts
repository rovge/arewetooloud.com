import bodyParser from "body-parser";
import express, { Express, Request, Response } from "express";
import http, { Server as HttpServer } from "http";
import socketio, { Server, Socket } from "socket.io";
import { IComplaint, ITooLoud } from "./models";

const app: Express = express();
const server: HttpServer = http.createServer(app);
const io: Server = socketio(server);

const PORT: number = Number(process.env.PORT) || 8000;

const complaints: IComplaint[] = [];

server.listen(PORT);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", (req: Request, res: Response) => {
  res.sendFile(__dirname + "/index.html");
});

app.get("/report", (req: Request, res: Response) => {
  res.sendFile(__dirname + "/report.html");
});

app.post("/submit-complaint", (req: Request, res: Response) => {
  const complaint: IComplaint = {
    name: req.body.name,
    message: req.body.message,
    date: new Date(req.body.date)
  };
  complaints.push(complaint);
  io.emit("new complaint", complaints);
});

app.get("/too-loud", (req: Request, res: Response) => {
  const tooLoud: ITooLoud = { tooLoud: complaints.length > 0 };
  res.send(tooLoud);
});

io.on("connection", (socket: Socket) => {
  // inefficient to update all clients when someone new connects but this will never be used
  socket.emit("new complaint", complaints);
});
