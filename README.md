# arewetooloud.com

An over engineered solution for knowing if we're too loud in our apartment!

### Installation
First the repo needs to be cloned and all the NPM packages need to be installed.

```bash
$ git clone https://gitlab.com/rovge/arewetooloud.com.git
$ cd arewetooloud.com
$ npm i
```
### Running
A development server can be spun up on port `8000` with the command:

```bash
$ npm run dev
```

A production server can be spun up on port `80` with the command:

```bash
$ npm run start
```

Alternatively, the project can be run from the Dockerfile by running the following commands:

```bash
$ docker build -t container-name
$ docker run -p 80:80 -i -t container-name
```
